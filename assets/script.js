// Phone validation for this foramt : (999) 999-9999
let length = 0
$("#phone").on('input',function(e){
    phoneValidation($(this),e)
})

$("#coolFormEdit #phone").on('input',function(e){
    phoneValidation($(this),e)
})

function phoneValidation(_this,e){
    const regex = /^(0*[()0-9][() 0-9 \-]*(\.[0-9]*)?|0*\.[()-0-9]*[1-9][0-9]*)$/
    if (!regex.test(e.target.value.toString())) {
        e.target.value = e.target.value.slice(0, -1)
    }
    let cond = length < e.target.value.length
    if(e.target.value.length == 1 && cond && e.target.value != '('){
        _this.val('(' + e.target.value)
    }
    if(e.target.value.length == 1 && !cond){
        _this.val('')
    }
    if(e.target.value.length == 4 && cond && e.target.value != ')'){
        _this.val(e.target.value + ')')
    }
    if(e.target.value.length == 5 && cond){
        _this.val(e.target.value + ' ')
    }
    if(e.target.value.length == 9 && cond){
        _this.val(e.target.value + '-')
    }
    length = e.target.value.length
}

// email validation
$('#coolForm').submit(function(){
    if(!validateForm($('#email').val())){ // call your validation function
        alert('Invalid Email Address!'); // remove this
        return false; // prevent the form to submit
    }
})

$('#coolFormEdit').submit(function(){
    if(!validateForm($('#coolFormEdit #email').val())){ // call your validation function
        alert('Invalid Email Address!'); // remove this
        return false; // prevent the form to submit
    }
})

function validateForm(input){
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input)){
        return true
    }
    return false
}

//appends an "active" class to .popup and .popup-content when the "Open" button is clicked
$(".open").on("click", function() {
    const arr = [];
    const parent = $(this).parents('tr')
    parent.find('td').each(function(){
        if($(this).attr("data-id") != 'action'){
            arr.push({
                value: $(this).text().toLowerCase(),
                key: $(this).attr("data-id")
            })
        }
    })
    const parent2 = $('.popup-content')
    parent2.find('input[type="checkbox"]').each(function(){
        $(this).prop('checked', false);
    })
    arr.forEach(el => {
        if(el.key != 'sports_fav'){
            parent2.find(`input[name="${el.key}"]`).val(el.value)
        }else{
            const sp = el.value.split(',')
            sp.forEach(val => {
                parent2.find(`input[id="${val}"]`).prop('checked', true);
            })
        }
    });
    $("#dataId").val($(this).parent().attr("data-action"))
    $(".popup-overlay, .popup-content").addClass("active");
});
  
//removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
$(".close").on("click", function() {
    $(".popup-overlay, .popup-content").removeClass("active");
});

$(".remove").click(function(){
    const parent = $(this).parent();
    const id = parent.attr("data-action");
    $.ajax('index.php', {
        type: 'POST',  // http method
        data: { action: 'remove', id },  // data to submit
        success: function (data, status, xhr) {
            if(data === 'success'){
                parent.parent().remove()
            }
            console.log('status: ' + status + ', data: ' + data);
        },
        error: function (jqXhr, textStatus, errorMessage) {
            console.log('Error' + errorMessage);
        }
    });
})