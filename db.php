<?php
define("SERVERNAME",  "localhost");
define("USERNAME",    "root");
define("PASSWORD",    "");
define("DBNAME",      "testform");
define("TABLE",       "testformdata");
define("HOME",        "http://localhost/simple-php-data-form");

// Create connection
try {
  $conn = new mysqli(SERVERNAME, USERNAME, PASSWORD, DBNAME);
} catch (\Throwable $th) {
  die('Caught exception: '. $th->getMessage());
}

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

include "server.php";
?>