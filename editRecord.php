<?php 
$data = $server->selectAll();

?>

<table id="editRecords" class="display">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Date of Birth</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Favorite sports</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
      <?php
        foreach ($data as $key) { ?>
            <tr>
                <td data-id="first_name"><?php echo $key['first_name'] ?></td>
                <td data-id="last_name"><?php echo $key['last_name'] ?></td>
                <td data-id="date_of_birth"><?php echo $key['date_of_birth'] ?></td>
                <td data-id="email"><?php echo $key['email'] ?></td>
                <td data-id="phone"><?php echo $key['phone'] ?></td>
                <td data-id="sports_fav"><?php echo ucfirst($key['sports_fav']) ?></td>
                <td data-id="action" data-action="<?php echo $key['id'] ?>">
                    <button class='open edit'>Edit</button>
                    <button class='remove'>Remove</button>
                </td>
            </tr>
       <?php }
      ?>
    </tbody>
</table>

<!--Creates the popup body-->
<div class="popup-overlay">
  <!--Creates the popup content-->
  <div class="popup-content">
    <h2>Edit Record</h2>
    <form id="coolFormEdit" action="" method="POST">
        <?php include "formTable.php"; ?>
        <input type="hidden" value="edit" name="action">
        <input id="dataId" type="hidden" value="" name="id">
        <button style="width:200px" class="saveTestForm">Save</button>
    </form>
    <!--popup's close button-->
    <button class="close">Close</button> </div>
</div>