<table class="formtable">
    <tr>
        <td> 
            <label for="firstName">First Name: </label>
        </td>
        <td>
            <input type="text" required minlength="3" id="firstName" name="first_name">
        </td> 
    </tr>
    <tr>
        <td>
            <label for="lastName">Last Name:</label> 
        </td>
        <td>
            <input type="text" required minlength="3" id="lastName" name="last_name">
        </td>
    </tr>
    <tr>
        <td>
            <label for="dateOfBirth">Date of Birth:</label>
        </td>
        <td>
            <input required type="date" id="dateOfBirth" name="date_of_birth">
        </td>
    </tr>
    <tr>
        <td>
            <label for="email">Email Address:</label>
        </td>
        <td>
            <input type="email" required id="email" name="email">
        </td>
    </tr>
    <tr>
        <td>
            <label for="phone">Phone:</label>
        </td>
        <td>
            <input pattern="[(][0-9]{3}[)] [0-9]{3}-[0-9]{4}" placeholder="(999) 999-9999" maxlength="14" required type="tel" id="phone" name="phone"><br>
        </td>
    </tr>
    <tr>
        <td>
            <label>Favorite sports:</label>
        </td>
        <td>
            <label for="karate">
                Karate
                <input type="checkbox" id="karate" value="karate" name="sports_fav[]">
            </label>
            <label for="box">
                Box
                <input type="checkbox" id="box" value="box" name="sports_fav[]">
            </label>
        </td>
    </tr>
</table>
