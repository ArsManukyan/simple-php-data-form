<?php 
    include 'db.php';
    if(isset($_POST['action'])){
        if($_POST['action'] === 'add'){
            unset($_POST['action']);
            $server->insert($_POST);
        }else if($_POST['action'] === 'edit'){
            unset($_POST['action']);
            $server->update($_POST);
        }else if($_POST['action'] === 'remove'){
            $server->remove($_POST);
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo HOME?>/assets/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
    <script>
        $( function() {
            $('#viewRecords').DataTable({
                ordering: false,
            });
            $('#editRecords').DataTable({
                ordering: false,
            });
            $( "#tabs" ).tabs();
        } );
    </script>
</head>
<body>

    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Add Records</a></li>
            <li><a href="#tabs-2">View Records</a></li>
            <li><a href="#tabs-3">Edit Records</a></li>
        </ul>
        <div id="tabs-1">
            <?php include "addRecord.php";?>
        </div>
        <div id="tabs-2">
            <?php include "viewRecord.php";?>
        </div>
        <div id="tabs-3">
            <?php include "editRecord.php";?>
        </div>
    </div>
    <script src="<?php echo HOME?>/assets/script.js"></script>
</body>
</html>