<?php
    class Server{
        public $db;
        public $table;
        public $homeURL;
        function __construct($conn,$table,$homeURL){
            $this->db = $conn;
            $this->table = $table;
            $this->homeURL = $homeURL;
        }

        function insert($data){
            $sports_fav = isset($data['sports_fav'])? $data['sports_fav'] : '';
            if($sports_fav != ''){
                $data['sports_fav'] = implode(',', $sports_fav);
            }
            $keys = array_keys($data);
            $keys = count($keys)? implode(',', $keys) : null;
            $values = array_values($data);
            $values = count($values)? implode("','", $values) : null;

            $sql = "INSERT INTO ".$this->table." (".$keys.") VALUES ('".$values."')";
            if ($this->db->query($sql) === TRUE) {
                echo "New record created successfully";
                header("Location: ".$this->homeURL.'#tabs-2');
                die();
            } else {
                echo "Error: " . $sql . "<br>" . $this->db->error;
            }
        }
        function update($data){
            $id = $data['id'];
            unset($data['id']);

            $sports_fav = isset($data['sports_fav'])? $data['sports_fav'] : '';
            if($sports_fav != ''){
                $data['sports_fav'] = strtolower(implode(',', $sports_fav));
            }else{
                $data['sports_fav'] = '';
            }
            $count = count($data);
            $ind = 0;
            $sql = "UPDATE ".$this->table. " Set ";
            foreach ($data as $key => $value) {
                $sql .= $key . " = '".$value."'";
                $sql .= (($count-1) > $ind) ? ', ' : ' '; 
                $ind++;
            }
            $sql .= " WHERE id=".$id;

            if ($this->db->query($sql) === TRUE) {
                echo "New record updated successfully";
                header("Location: ".$this->homeURL.'#tabs-3');
                die();
            } else {
                echo "Error: " . $sql . "<br>" . $this->db->error;
            }

        }
        function remove($data){
            $id = isset($data['id'])? $data['id'] : 0;
            $sql = "DELETE FROM ".$this->table." WHERE id=".$id;
            if ($this->db->query($sql) === TRUE) {
                echo 'success';
                die();
            } else {
                echo "Error: " . $sql . "<br>" . $this->db->error;
            }
        }
        function selectAll(){
            $sql = "Select * from ".$this->table." ORDER BY id DESC";
            $data = $this->db->query($sql);
            if($data){
                return $data->fetch_all(MYSQLI_ASSOC);
            }
            return [];
        }
    }
    $server = new Server($conn,TABLE,HOME);

?>