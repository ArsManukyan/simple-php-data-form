<?php $data = $server->selectAll(); ?>

<table id="viewRecords" class="display">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Date of Birth</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Favorite sports</th>
        </tr>
    </thead>
    <tbody>
      <?php
        foreach ($data as $key) { ?>
            <tr>
                <td><?php echo $key['first_name'] ?></td>
                <td><?php echo $key['last_name'] ?></td>
                <td><?php echo $key['date_of_birth'] ?></td>
                <td><?php echo $key['email'] ?></td>
                <td><?php echo $key['phone'] ?></td>
                <td><?php echo ucfirst($key['sports_fav']) ?></td>
            </tr>
       <?php }
      ?>
    </tbody>
</table>